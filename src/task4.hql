-- Get the posts belonging to each of top 10 users by score
create view top10_users as 
  SELECT 
    OwnerUserId, 
    score 
  FROM posts 
  ORDER BY score 
  DESC LIMIT 10;

create view top10_users_posts as 
  SELECT 
    p.Id, 
    p.OwnerUserId, 
    p.Body 
  from posts p 
  JOIN top10_users u 
  where p.OwnerUserId == u.OwnerUserId;

-- Commands below from line 21 to 22 referenced from : https://hivemall.incubator.apache.org/userguide/getting_started/installation.html
-- Install Hivemall to use tokenize functions and others
add jar /home/kyle599747/hivemall-core-0.4.2-rc.2-with-dependencies.jar;
source /home/kyle599747/define-all.hive;

-- Code below from line 6 to 67 referenced from : https://hivemall.incubator.apache.org/userguide/ft_engineering/tfidf.html
create temporary macro max2(x INT, y INT)
if(x>y,x,y);

create temporary macro tfidf(tf FLOAT, df_t INT, n_docs INT)
tf * (log(10, CAST(n_docs as FLOAT)/max2(1,df_t)) + 1.0);

create or replace view posts_exploded
as
select
  Id as docid, 
  OwnerUserId,
  word
from
  top10_users_posts LATERAL VIEW explode(tokenize(Body,true)) t as word
where
  not is_stopword(word);

-- Get term frequency
create or replace view term_frequency 
as
select
  docid, 
  word,
  OwnerUserId,
  freq
from (
    select
    docid,
    OwnerUserId,
    tf(word) as word2freq
    from
    posts_exploded
    group by
    docid,OwnerUserId
) t 
LATERAL VIEW explode(word2freq) t2 as word, freq;

-- Get document frequency
create or replace view document_frequency
as
select
  word, 
  count(distinct docid) docs
from
  posts_exploded
group by
  word;

select count(distinct docid) from posts_exploded;
set hivevar:n_docs=3;

-- Calculate tf-idf
create or replace view tfidf
as
select
  tf.docid,
  tf.word, 
  tf.OwnerUserId,
  tfidf(tf.freq, df.docs, ${n_docs}) as tfidf
from
  term_frequency tf 
  JOIN document_frequency df ON (tf.word = df.word)
order by 
  tfidf desc;

-- Code below from line 69 to 72 eferenced from: https://stackoverflow.com/questions/9390698/hive-getting-top-n-records-in-group-by-query
-- Get top 10 terms for each of top 10 users by score
set hive.cli.print.header=true;
select OwnerUserId, word, tfidf,rank 
from (
  select 
    OwnerUserId, 
    word, 
    tfidf, 
    rank() over (PARTITION BY owneruserid ORDER by tfidf DESC) as rank 
  from tfidf
) ranked_tfidf where ranked_tfidf.rank <= 10;