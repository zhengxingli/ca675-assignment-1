-- Create table
create table assignment1.posts (
    Id int,
    PostTypeId int,
    AcceptedAnswerId int,
    ParentId int,
    CreationDate timestamp,
    DeletionDate timestamp,
    Score int,
    ViewCount int,
    Body string,
    OwnerUserId int,
    OwnerDisplayName string,
    LastEditorUserId int,
    LastEditorDisplayName string,
    LastEditDat timestamp,
    LastActivityDate timestamp,
    Titl string,
    Tags string,
    AnswerCount int,
    CommentCount int,
    FavoriteCount int,
    ClosedDate timestamp,
    CommunityOwnedDate timestamp,
    ContentLicense string
) 
COMMENT 'posts'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ',';

-- Load data
LOAD DATA INPATH 'hdfs://cluster-30b9-m/posts_data/output_data' overwrite INTO TABLE assignment1.posts;

-- Get top 10 posts by post scores
SELECT * from posts ORDER BY score DESC LIMIT 10;

-- Get top 10 users by post scores
SELECT OwnerUserId AS UserId, OwnerDisplayName As UserName, Score FROM posts ORDER BY score DESC LIMIT 10;

-- Get the number of distinct users, who used the word “Hadoop” in one of their posts
SELECT COUNT(DISTINCT(OwnerUserId)) as DistinctUserCount FROM posts WHERE body LIKE '%Hadoop%';